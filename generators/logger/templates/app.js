'use strict'

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.Logger()

// let connectionString

/**
 * Emitted when device data is received.
 * This is the event to listen to in order to get real-time data feed from the connected devices.
 * * @param {string} logData The log data represented as string. Can be a JSON String.
 */
plugin.on('log', (logData) => {
  // TODO: Send the log (title + description) to the logging service using the initialized connection..
  console.log(logData)
})

/**
 * Emitted when the platform bootstraps the plugin.
 * The plugin should listen once and execute its init process.
 */
plugin.once('ready', () => {
  /**
   *
   *  Initialize the connection using the plugin.config. See config.json
   *  You can customize config.json based on the needs of your plugin.
   *  Reekoh will inject these configuration parameters as plugin.config when the platform bootstraps the plugin.
   *
   *
   * let connectionString = plugin.config.connString
   * connection = loggingService.connect(connectionString)
   *
   *
   *  Note: Config Names are based on what you specify on the config.json.
   */

  // TODO: Initialize the connection to the logging service here.
  plugin.log('Logger has been initialized.')
})

module.exports = plugin