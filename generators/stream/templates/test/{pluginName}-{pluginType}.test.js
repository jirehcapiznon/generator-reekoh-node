/* global describe, it, after, before */
'use strict'

const should = require('should')

let _app = null

describe('<Plugin Name>', () => {
  before('init', function () {
    // TODO: initialize all needed pre-test requirements i.e. process.env variables or connecting to some required services (e.g. rabbitmq)
  })

  after('terminate', function () {
    // TODO: gracefully terminate initialized services (e.g. rabbitmq)
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(10000)

      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#data', function () {
    it('should process the data', (done) => {
      this.timeout(10000)
      // TODO:
    })
  })
})

